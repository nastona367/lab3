from django.urls import path
from .views import *

urlpatterns = [
    path('', main, name='main'),
    path('1', some, name='some'),
    path('2', some2, name='some2'),
    path('3', some3, name='some3')]
