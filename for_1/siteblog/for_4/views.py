from django.http import HttpResponse, FileResponse, HttpResponseRedirect, \
    HttpResponseNotAllowed, JsonResponse
from django.shortcuts import render
from django.templatetags.static import static

from django.views import View

from django.template import loader


class MyView(View):

    def get(self, request):
        if request.GET.get('type') == "file":
            return FileResponse(open(static('img/user.png'), "rb+"), )
        elif request.GET.get('type') == "json":
            return JsonResponse({i: i + i for i in range(1, 20)}, safe=False)
        elif request.GET.get('type') == "redirect":
            return HttpResponseRedirect("http://127.0.0.1:8000/admin")
        else:
            return HttpResponseNotAllowed("You shall not pass!!!")


def some(request):
    return JsonResponse({i: i + i for i in range(1, 20)}, safe=False)


def some2(request):
    return HttpResponseRedirect("http://www.google.com")


def some3(request):
    return HttpResponse("This is text from backend to user interface")


def main(request):
    return render(request, 'pages/some.html')
