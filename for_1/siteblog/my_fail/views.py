from django.shortcuts import render
from django.http import HttpResponseRedirect


def some(request):
    lets_do_it = [{'priority': 100, 'task': 'Составить список дел'}, {'priority': 150, 'task': 'Изучать Django'},
                  {'priority': 1, 'task': 'Подумать о смысле жизни'}]
    return render(request, 'pages/main.html', {'page': lets_do_it})


def main(request):
    return render(request, 'pages/main.html')
