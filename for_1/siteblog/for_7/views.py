from django.shortcuts import render
from django.http import HttpResponseRedirect


def some(request):
    lets_do_it = [{'name': 'Шаддам IV', 'surname': 'Коррино'},
                  {'name': 'Пол', 'surname': 'Атрейдес'},
                  {'name': 'Франклин', 'surname': 'Герберт'}]
    return render(request, 'pages/some.html', {'page': lets_do_it})


def main(request):
    return render(request, 'pages/some.html')
