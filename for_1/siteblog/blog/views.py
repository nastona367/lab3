from django.shortcuts import render
from django.http import HttpResponseRedirect


def some(request):
    lets_do_it = [{'priority': 100, 'task': 'Составить список дел'}, {'priority': 150, 'task': 'Изучать Django'},
                  {'priority': 1, 'task': 'Подумать о смысле жизни'}]
    for a in range(0, len(lets_do_it) - 1):
        for i in range(0, len(lets_do_it)-1):
            if lets_do_it[i].get('priority') > lets_do_it[i+1].get('priority'):
                lets_do_it[i], lets_do_it[i+1] = lets_do_it[i+1], lets_do_it[i]
    return render(request, 'pages/main.html', {'page': lets_do_it})


def main(request):
    return render(request, 'pages/main.html')
