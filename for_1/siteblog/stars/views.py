from django.shortcuts import render
from django.http import HttpResponseRedirect


def luk(request):
    return render(request, 'pages/for_1.html')


def lea(request):
    return render(request, 'pages/for_2.html')


def han(request):
    return render(request, 'pages/for_3.html')


def main(request):
    return render(request, 'pages/main1.html')
