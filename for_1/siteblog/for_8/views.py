from django.shortcuts import render
from django.http import HttpResponseRedirect


def some(request):
    atest_question_list = [{'id': 1, 'question_text': 'В чем смысл жизни?'},
                                        {'id': 2, 'question_text': 'Что первично, дух или материя?'},
                                        {'id': 3, 'question_text': 'Существует ли свобода воли?'}]

    return render(request, 'pages/main.html', {'page': atest_question_list})


def main(request):
    return render(request, 'pages/main.html')
