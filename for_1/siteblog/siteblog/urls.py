from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('for_1/', include('blog.urls')),
    path('for_2/', include('stars.urls')),
    path('for_3/', include('for_3.urls')),
    path('for_4/', include('for_4.urls')),
    path('for_5/', include('my_fail.urls')),
    path('for_6/', include('stars2.urls')),
    path('for_7/', include('for_7.urls')),
    path('for_8/', include('for_8.urls'))
]
